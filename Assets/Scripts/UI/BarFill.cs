﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BarFill : MonoBehaviour
    {
        [SerializeField] private int barWidth;
        [SerializeField] private RectTransform mask;
        [SerializeField] private Image fill;
        [SerializeField] private bool changeFill;
        [SerializeField] private Gradient gradient;
        [SerializeField] private float previousWidth, targetWidth;

        public static bool DelayBars = false;
    
        private void Awake()
        {
            previousWidth = mask.sizeDelta.x;
        }

        public void SetBar(int percentage)
        {
            targetWidth = barWidth * percentage / 100f;
            if (Mathf.Abs(previousWidth - targetWidth) < 1) return;
            previousWidth = mask.sizeDelta.x; // Don't double trigger a change
            StopAllCoroutines(); // Stop in case of an override
            StartCoroutine(Scale());
        }

        private IEnumerator Scale()
        {
            previousWidth = mask.sizeDelta.x;
            if (DelayBars) yield return new WaitForSeconds(1.2f);
            for (float t = 0; t < 0.3f; t += Time.deltaTime)
            {
                mask.sizeDelta = new Vector2(Mathf.Lerp(previousWidth, targetWidth, t/0.3f), 0);
                if (changeFill) fill.color = gradient.Evaluate(mask.sizeDelta.x/barWidth);
                yield return null;
            }
            mask.sizeDelta = new Vector2(targetWidth, 0);
            if (changeFill) fill.color = gradient.Evaluate(mask.sizeDelta.x/barWidth);
        }
    }
}
