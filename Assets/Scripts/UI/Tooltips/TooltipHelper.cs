﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public interface TooltipHelper
{
    void UpdateTooltip();
}
